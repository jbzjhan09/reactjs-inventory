from bson.objectid import ObjectId
class CartDB:
	def __init__(self, conn):
		self.conn = conn

	def getCart(self, username):
		return self.conn.aggregate([{'$group':{'_id': '$inventory_id','sum': {'$sum': '$quantity'}, \
			'count':{'$sum': 1}}}, \
			{'$lookup': {'from': 'inventory', 'localField': '_id', 'foreignField': '_id', \
			'as': 'inventory_details'}}, {'$unwind': '$inventory_details'}])

	def insertCart(self, inventory_id, quantity, username):
		self.conn.insert({'inventory_id': ObjectId(inventory_id), 'username': username, 'quantity': quantity})

