from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session
from datetime import datetime
from bson import json_util, ObjectId
import json
from app.modules.inventory.cart_form import CartForm

cart_blueprint = Blueprint('cart', __name__)

def toJSON(data):
	return json.dumps(data, default=json_util.default)

@cart_blueprint.route('/push', methods=['POST'])
def push_to_cart():
	inventory_id = request.json['inventory_id']
	quantity = int(request.json['quantity'])
	g.cart.insertCart(inventory_id, quantity, session['username'])
	return cart_list()

@cart_blueprint.route('/cart', methods=['POST'])
def cart_list():
	cart = g.cart.getCart(session['username'])
	json_cart = []
	for i in cart:
		json_cart.append(i)
	return toJSON(json_cart)

@cart_blueprint.route('/cart', methods=['GET'])
def goToCart():
	return render_template('/inventory/cart.html')
