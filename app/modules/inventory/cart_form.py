from flask_wtf import Form
from wtforms import DecimalField
from wtforms import validators, ValidationError

class CartForm(Form):
	quantity = DecimalField('Quantity', [validators.Required('Quantity is a required field.')])