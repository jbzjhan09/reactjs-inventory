Inventory = React.createClass({
	getInitialState: function(){
		return {
			cart: [],
			product: []
		}
	},
	componentDidMount: function(){
		$.ajax({
			url: '/inventory',
			type: 'POST',
			success: function(reply){
				this.setState({product: JSON.parse(reply)});
			}.bind(this),
			error: function(error){
				alert('Something went wrong while fetching inventory.')
			}.bind(this)
		});
		$.ajax({
			url: '/cart',
			type: 'POST',
			success: function(reply){
				this.setState({cart: JSON.parse(reply)});
			}.bind(this),
			error: function(error){
				alert('Something went wrong while fetching cart.')
			}
		});
	},
	addToCart: function(inventory_id, quantity){
		var inventoryObj = {}
		inventoryObj.inventory_id = inventory_id
		inventoryObj.quantity = quantity
		$.ajax({
			url: '/push',
			type: 'POST',
			data: JSON.stringify(inventoryObj, null, '\t'),
			contentType: 'application/json;charset=UTF-8',
			success: function(reply){
				alert('Product added to cart.')
				this.setState({cart: JSON.parse(reply)});
			}.bind(this),
			error: function(error){
				alert('Something went wrong while pushing to cart.')
			}.bind(this)
		});
	},
	eachProduct: function(product, i){
		var id = []
		id.push(product._id)
		var _id = id[0].$oid
		return (<Product price={product.price} id={_id} addToCart={this.addToCart}>{product.name}</Product>)
	},
	render: function(){
		return(<div>
					<div className='col-md-12 col-sm-12 sol-xs-12'>
						<h1>Inventory</h1>
						<table className='table'>
							<thead>
								<tr>
									<th>Product</th>
									<th>Price</th>
									<th>Enter Quantity</th>
									<th>Push to Cart</th>
								</tr>
							</thead>
							<tbody>
								{this.state.product.map(this.eachProduct)}
							</tbody>
						</table>
					</div>
				   	<Cart cart={this.state.cart} />
			   </div>)
	}
});
ReactDOM.render(<Inventory />, document.getElementById('inventory'))

