var IntlMixin       = ReactIntl.IntlMixin;
var FormattedNumber = ReactIntl.FormattedNumber;
var IntlProvider = ReactIntl.IntlProvider;
var total = 0

Cart = React.createClass({
	mixins: [IntlMixin],
	getInitialState: function(){
		return{
			cart_list: []
		}
	},
	renderCart: function(c, i){
		var inventory = []
		inventory.push(c.inventory_details)
		total += (c.sum * inventory[0].price)
		return(
			<tr>
				<td>{inventory[0].name}</td>
				<td>{c.sum}</td>
				<td><FormattedNumber value={inventory[0].price} style='currency' currency='USD'/></td>
				<td><FormattedNumber value={c.sum * inventory[0].price} style='currency' currency='USD'/></td>
			</tr>
			)
	},
	render: function(){
		total = 0
		return (
		<IntlProvider locale="en">
			<div className='col-md-12 col-sm-12 col-xs-12'>
				<h1>Cart</h1>
				<table className='table table-striped'>
					<thead>
						<tr>
							<th>Product</th>
							<th>Quantity</th>
							<th>Price</th>
							<th>SubTotal</th>
						</tr>
					</thead>
					<tbody>
						{this.props.cart.map(this.renderCart)}
						<tr>
							<td colSpan='3'>Total</td>
							<td>
								<FormattedNumber value={total} style='currency' currency='USD'/>
							</td>
						</tr>
					</tbody>
				</table>	
			</div>
		</IntlProvider>
		)
		
	}
});
