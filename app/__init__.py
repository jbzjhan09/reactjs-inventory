from flask import Flask
from flask import g

from modules import default
from modules import users
from modules import inventory
from modules.inventory import cart

from pymongo import MongoClient

app = Flask(__name__)

def get_main_db():
    client = MongoClient('mongodb://localhost:27017/')
    maindb = client.postsdb
    return maindb

def get_inventory_db():
    client = MongoClient()
    inventorydb = client.inventorydb
    return inventorydb

@app.before_request
def before_request():
    mainDb = get_main_db()
    inventoryDb =  get_inventory_db()
    g.usersdb = users.UserDB(conn=mainDb.users)
    g.inventorydb = inventory.InventoryDB(conn=inventoryDb.inventory)
    g.cart = inventory.CartDB(conn=inventoryDb.cart)
    
@app.teardown_request
def teardown_request(exception):
    postdb = getattr(g, 'postdb', None)
    if postdb is not None:
        postdb.close()
    userdb = getattr(g, 'userdb', None)
    if userdb is not None:
        userdb.close()


app.register_blueprint(default.mod)
app.register_blueprint(inventory.inventory_blueprint)
app.register_blueprint(cart.cart_blueprint)